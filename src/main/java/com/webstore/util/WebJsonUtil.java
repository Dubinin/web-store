package com.webstore.util;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;

public class WebJsonUtil {
    private static final Gson GSON = new Gson();

    public static void putJson(final HttpServletResponse response, final Object entity) throws IOException {
        PrintWriter out = response.getWriter();
        response.setContentType("application/json");
        response.setCharacterEncoding("UTF-8");
        out.print(GSON.toJson(entity));
        out.flush();
    }
}
