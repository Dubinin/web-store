package com.webstore.util;

public class Constant {
    public static final String FIRST_NAME = "first_name";
    public static final String LAST_NAME = "last_name";
    public static final String EMAIL = "email";
    public static final String ADDRESS = "address";
    public static final String PHONE_NUMBER = "phone_number";
    public static final String PASSWORD = "password";

    public static final String SHOP_USER = "shop_user";
}
