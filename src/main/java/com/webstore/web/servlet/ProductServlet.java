package com.webstore.web.servlet;


import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.webstore.entity.Product;
import com.webstore.service.ProductService;
import com.webstore.service.impl.ProductServiceImpl;
import com.webstore.util.WebJsonUtil;

public class ProductServlet extends HttpServlet {
    private ProductService productService = new ProductServiceImpl();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        final String idParam = req.getParameter("id");
        final String categoryId = req.getParameter("category_id");

        if (idParam != null && !idParam.isEmpty()) {
            WebJsonUtil.putJson(resp, productService.get(Integer.valueOf(idParam)));
        }else if(categoryId != null && !categoryId.isEmpty()) {
            final List<Product> products = productService.getProductsByCategoryId(Integer.valueOf(categoryId));
            WebJsonUtil.putJson(resp, products);
        } else {
            final List<Product> products = productService.getAll();
            WebJsonUtil.putJson(resp, products);
        }

    }
}
