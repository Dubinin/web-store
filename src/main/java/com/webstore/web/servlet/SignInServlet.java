package com.webstore.web.servlet;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.codec.digest.DigestUtils;

import com.webstore.entity.Role;
import com.webstore.entity.User;
import com.webstore.service.UserService;
import com.webstore.service.impl.UserServiceImpl;
import com.webstore.util.Constant;

public class SignInServlet extends HttpServlet {
    private final UserService userService = new UserServiceImpl();

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) {
        final String firstName = req.getParameter(Constant.FIRST_NAME);
        final String lastName = req.getParameter(Constant.LAST_NAME);
        final String email = req.getParameter(Constant.EMAIL);
        final String address = req.getParameter(Constant.ADDRESS);
        final String phoneNumber = req.getParameter(Constant.PHONE_NUMBER);
        final String password = DigestUtils.sha256Hex(req.getParameter(Constant.PASSWORD));
        final Role role = Role.USER;

        userService.create(new User(firstName, lastName, email, address, phoneNumber, password, role));

        final User user = userService.findUser(email, password);
        req.getSession().setAttribute(Constant.SHOP_USER, user);
        resp.addCookie(new Cookie(Constant.SHOP_USER, String.valueOf(user.getId())));
    }
}