package com.webstore.web.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.codec.digest.DigestUtils;

import com.webstore.entity.User;
import com.webstore.service.UserService;
import com.webstore.service.impl.UserServiceImpl;
import com.webstore.util.Constant;
import com.webstore.util.WebJsonUtil;

public class LogInServlet extends HttpServlet {
    private final UserService userService = new UserServiceImpl();

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) {
        final String email = req.getParameter(Constant.EMAIL);
        final String password = req.getParameter(Constant.PASSWORD);
        final User user = userService.findUser(email, DigestUtils.sha256Hex(password));

        req.getSession().setAttribute(Constant.SHOP_USER, user);
        resp.addCookie(new Cookie(Constant.SHOP_USER, String.valueOf(user.getId())));
    }
}
