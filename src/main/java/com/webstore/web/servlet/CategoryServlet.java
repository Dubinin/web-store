package com.webstore.web.servlet;


import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.webstore.entity.Category;
import com.webstore.service.CategoryService;
import com.webstore.service.impl.CategoryServiceImpl;
import com.webstore.util.WebJsonUtil;

public class CategoryServlet extends HttpServlet {
    private CategoryService categoryService = new CategoryServiceImpl();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        final String idParam = req.getParameter("id");
        if (idParam == null) {
            final List<Category> categories = categoryService.getAllMainCategories();
            WebJsonUtil.putJson(resp, categories);
        } else {
            final Integer id = Integer.valueOf(idParam);
            WebJsonUtil.putJson(resp, categoryService.getChildrenCategories(id));
        }
    }
}
