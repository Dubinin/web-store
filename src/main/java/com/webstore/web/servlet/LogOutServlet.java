package com.webstore.web.servlet;

import java.io.IOException;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.webstore.service.UserService;
import com.webstore.service.impl.UserServiceImpl;
import com.webstore.util.Constant;

public class LogOutServlet extends HttpServlet {
    private final UserService userService = new UserServiceImpl();

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        req.getSession().removeAttribute(Constant.SHOP_USER);
    }
}
