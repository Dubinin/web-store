package com.webstore.web.servlet;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.codec.digest.DigestUtils;

import com.google.gson.Gson;
import com.google.gson.internal.LinkedTreeMap;
import com.webstore.entity.OrderItem;
import com.webstore.entity.User;
import com.webstore.service.CartService;
import com.webstore.service.UserService;
import com.webstore.service.impl.CartServiceImpl;
import com.webstore.service.impl.UserServiceImpl;
import com.webstore.util.Constant;

public class CartServlet extends HttpServlet {
    private static final Gson GSON = new Gson();
    private static final CartService CART_SERVICE = new CartServiceImpl();

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) {
        final User user = (User) req.getSession().getAttribute(Constant.SHOP_USER);
        final List<LinkedTreeMap<String, Double>> cart = GSON.fromJson(req.getParameter("cart"), List.class);
        final List<OrderItem> orderItems = toOrderItems(cart);

        if (user != null) {
            CART_SERVICE.createOrder(user.getId(), orderItems);
        }

        //ToDo make bad response
    }

    private List<OrderItem> toOrderItems(final List<LinkedTreeMap<String, Double>> cart) {
        final List<OrderItem> orderItems = new ArrayList<>();
        for (LinkedTreeMap<String, Double> linkedTreeMap : cart) {

            int productId = linkedTreeMap.get("id").intValue();
            int count = linkedTreeMap.get("count").intValue();
            orderItems.add(new OrderItem(productId, count));
        }
        return orderItems;
    }
}
