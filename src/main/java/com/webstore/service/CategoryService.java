package com.webstore.service;


import java.sql.Connection;
import java.util.List;

import com.webstore.entity.Category;
import com.webstore.entity.Product;

public interface CategoryService {
    Category get(final int id);
    List<Category> getChildrenCategories(final int parentCategoryId);
    List<Category> getAllMainCategories();
}
