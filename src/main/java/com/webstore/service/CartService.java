package com.webstore.service;

import java.util.List;

import com.webstore.entity.OrderItem;

public interface CartService {
    int createOrder(int userId, List<OrderItem> orderItems);
}
