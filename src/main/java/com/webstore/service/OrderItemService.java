package com.webstore.service;

import com.webstore.entity.Order;
import com.webstore.entity.OrderItem;

public interface OrderItemService {
    int create(final OrderItem orderItem);
}
