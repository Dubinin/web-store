package com.webstore.service;

import com.webstore.entity.User;

public interface UserService {
    User findUser(final String email, final String password);

    boolean create(final User user);
}
