package com.webstore.service;

import java.sql.Connection;
import java.sql.SQLException;

import com.webstore.entity.Order;
import com.webstore.entity.User;

public interface OrderService {
    int create(final int userId);
}
