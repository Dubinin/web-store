package com.webstore.service;


import java.sql.Connection;
import java.util.List;

import com.webstore.entity.Product;

public interface ProductService {
    List<Product> getAll();

    Product get(final int id);

    List<Product> getProductsByCategoryId(final int categoryId);
}
