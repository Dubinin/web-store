package com.webstore.service.impl;

import java.sql.SQLException;

import com.webstore.entity.User;
import com.webstore.repository.ConnectionProvider;
import com.webstore.repository.UserRepository;
import com.webstore.repository.impl.UserRepositoryImpl;
import com.webstore.service.UserService;

public class UserServiceImpl implements UserService {
    private final UserRepository userRepository = new UserRepositoryImpl();

    @Override
    public User findUser(String email, String password) {
        try {
            return userRepository.findUser(email, password, ConnectionProvider.getConnection());
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public boolean create(User user) {
        try {
            return userRepository.create(user, ConnectionProvider.getConnection());
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }
}
