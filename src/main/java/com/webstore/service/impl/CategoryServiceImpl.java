package com.webstore.service.impl;


import java.sql.SQLException;
import java.util.Collections;
import java.util.List;

import com.webstore.entity.Category;
import com.webstore.repository.CategoryRepository;
import com.webstore.repository.ConnectionProvider;
import com.webstore.repository.impl.CategoryRepositoryImpl;
import com.webstore.service.CategoryService;

public class CategoryServiceImpl implements CategoryService{
    private final CategoryRepository categoryRepository = new CategoryRepositoryImpl();

    @Override
    public Category get(int id) {
        try {
            return categoryRepository.getCategory(id, ConnectionProvider.getConnection());
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public List<Category> getChildrenCategories(int parentCategoryId) {
        try {
            return categoryRepository.getChildrenCategories(parentCategoryId, ConnectionProvider.getConnection());
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return Collections.emptyList();
    }

    @Override
    public List<Category> getAllMainCategories() {
        try {
            return categoryRepository.getAllMainCategories(ConnectionProvider.getConnection());
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return Collections.emptyList();
    }
}
