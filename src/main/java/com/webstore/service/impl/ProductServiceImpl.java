package com.webstore.service.impl;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Collections;
import java.util.List;

import com.webstore.entity.Product;
import com.webstore.repository.ProductRepository;
import com.webstore.repository.ConnectionProvider;
import com.webstore.repository.impl.ProductRepositoryImpl;
import com.webstore.service.ProductService;

public class ProductServiceImpl implements ProductService {
    private final ProductRepository productRepository = new ProductRepositoryImpl();

    @Override
    public List<Product> getAll() {
        try {
            final List<Product> products = productRepository.getAll(ConnectionProvider.getConnection());
            for(Product product : products) {
                product.setImages(productRepository.getProductImages(product.getId(), ConnectionProvider.getConnection()));
            }
            return products;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return Collections.emptyList();
    }

    @Override
    public Product get(int id) {
        try {
            final Product product = productRepository.getProduct(id, ConnectionProvider.getConnection());
            if (product != null) {
                product.setImages(productRepository.getProductImages(id, ConnectionProvider.getConnection()));
            }

            return product;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public List<Product> getProductsByCategoryId(int categoryId) {
        try {
            final List<Product> products = productRepository.getProductsByCategoryId(categoryId, ConnectionProvider.getConnection());
            for(Product product : products) {
                product.setImages(productRepository.getProductImages(product.getId(), ConnectionProvider.getConnection()));
            }
            return products;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return Collections.emptyList();
    }
}
