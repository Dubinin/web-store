package com.webstore.service.impl;

import java.sql.SQLException;

import com.webstore.entity.Order;
import com.webstore.repository.ConnectionProvider;
import com.webstore.repository.OrderRepository;
import com.webstore.repository.impl.OrderRepositoryImpl;
import com.webstore.service.OrderService;

public class OrderServiceImpl implements OrderService {
    private final OrderRepository orderRepository = new OrderRepositoryImpl();

    @Override
    public int create(int userId) {
        try {
            return orderRepository.create(userId, ConnectionProvider.getConnection());
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return -1;
    }
}
