package com.webstore.service.impl;

import java.util.List;

import com.webstore.entity.OrderItem;
import com.webstore.service.CartService;
import com.webstore.service.OrderItemService;
import com.webstore.service.OrderService;

public class CartServiceImpl implements CartService {
    private final OrderService orderService = new OrderServiceImpl();
    private final OrderItemService orderItemService = new OrderItemServiceImpl();

    @Override
    public int createOrder(int userId, List<OrderItem> orderItems) {
        final int orderId = orderService.create(userId);
        if (orderId > -1) {
            for (OrderItem orderItem : orderItems) {
                orderItem.setOrderId(orderId);
                orderItemService.create(orderItem);
            }
        }
        return orderId;
    }
}
