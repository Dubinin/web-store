package com.webstore.service.impl;

import java.sql.SQLException;

import com.webstore.entity.OrderItem;
import com.webstore.repository.ConnectionProvider;
import com.webstore.repository.OrderItemRepository;
import com.webstore.repository.impl.OrderItemRepositoryImpl;
import com.webstore.service.OrderItemService;

public class OrderItemServiceImpl implements OrderItemService {
    private final OrderItemRepository orderItemRepository = new OrderItemRepositoryImpl();
    @Override
    public int create(OrderItem orderItem) {
        try {
            return orderItemRepository.create(orderItem, ConnectionProvider.getConnection());
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return -1;
    }
}
