package com.webstore.entity;

public enum Role {
    USER, ADMINISTRATOR
}
