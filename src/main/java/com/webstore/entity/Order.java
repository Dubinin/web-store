package com.webstore.entity;

public class Order {
    private int id;
    private int userId;
    private OrderState orderState;

    public Order(int id, int userId, OrderState orderState) {
        this.id = id;
        this.userId = userId;
        this.orderState = orderState;
    }

    public Order(int userId, OrderState orderState) {
        this.userId = userId;
        this.orderState = orderState;
    }

    public int getId() {
        return id;
    }

    public int getUserId() {
        return userId;
    }

    public OrderState getOrderState() {
        return orderState;
    }
}
