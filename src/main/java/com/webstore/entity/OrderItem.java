package com.webstore.entity;

public class OrderItem {
    private int id;
    private int orderId;
    private int productId;
    private int count;

    public OrderItem(int id, int orderId, int productId, int count) {
        this.id = id;
        this.orderId = orderId;
        this.productId = productId;
        this.count = count;
    }

    public OrderItem(int productId, int count) {
        this.productId = productId;
        this.count = count;
    }

    public int getId() {
        return id;
    }

    public int getOrderId() {
        return orderId;
    }

    public int getProductId() {
        return productId;
    }

    public int getCount() {
        return count;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setOrderId(int orderId) {
        this.orderId = orderId;
    }

    public void setProductId(int productId) {
        this.productId = productId;
    }

    public void setCount(int count) {
        this.count = count;
    }
}
