package com.webstore.entity;

import java.util.List;
import java.util.Objects;

public class Product {
    private int id;
    private String name;
    private int category_id;
    private int price;
    private String description;
    private List<String> images;
    private boolean inStock;

    public Product(int id, String name, int category_id, int price, String description, boolean inStock) {
        this.id = id;
        this.name = name;
        this.category_id = category_id;
        this.price = price;
        this.description = description;
        this.inStock = inStock;
    }

    public boolean isInStock() {
        return inStock;
    }

    public void setInStock(boolean inStock) {
        this.inStock = inStock;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getCategory_id() {
        return category_id;
    }

    public void setCategory_id(int category_id) {
        this.category_id = category_id;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<String> getImages() {
        return images;
    }

    public void setImages(List<String> images) {
        this.images = images;
    }
}
