package com.webstore.repository;

import java.sql.Connection;
import java.sql.SQLException;

import com.webstore.entity.User;

public interface UserRepository {
    User findUser(final String email, final String password, final Connection connection) throws SQLException;

    boolean create(final User user, final Connection connection) throws SQLException;
}
