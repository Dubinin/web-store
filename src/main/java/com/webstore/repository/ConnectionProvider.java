package com.webstore.repository;

import java.sql.Connection;
import java.sql.SQLException;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

public class ConnectionProvider {
    public static final String JAVA_COMP_ENV_JDBC_SHOP_DB = "java:/comp/env/jdbc/web_shop";

    public static synchronized Connection getConnection() {
        Connection connection = null;
        try {
            InitialContext cxt = new InitialContext();
            DataSource datasource = (javax.sql.DataSource) cxt.lookup(JAVA_COMP_ENV_JDBC_SHOP_DB);
            connection = datasource.getConnection();
        } catch (NamingException | SQLException e) {
            e.printStackTrace();
        }
        return connection;
    }
}
