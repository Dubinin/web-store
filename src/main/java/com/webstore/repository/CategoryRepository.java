package com.webstore.repository;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

import com.webstore.entity.Category;

public interface CategoryRepository {
    Category getCategory(final int id, final Connection connection) throws SQLException;
    List<Category> getChildrenCategories(final int parentCategoryId, final Connection connection) throws SQLException;
    List<Category> getAllMainCategories(final Connection connection) throws SQLException;
}
