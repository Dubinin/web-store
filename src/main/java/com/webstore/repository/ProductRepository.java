package com.webstore.repository;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

import com.webstore.entity.Product;

public interface ProductRepository {
    Product getProduct(final int id, final Connection connection) throws SQLException;
    List<Product> getProductsByCategoryId(final int categoryId, final Connection connection) throws SQLException;
    List<Product> getAll(final Connection connection) throws SQLException;
    List<String> getProductImages(final int productId, final Connection connection) throws SQLException;
}
