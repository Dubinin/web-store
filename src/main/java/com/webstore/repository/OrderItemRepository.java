package com.webstore.repository;

import java.sql.Connection;
import java.sql.SQLException;

import com.webstore.entity.OrderItem;

public interface OrderItemRepository {
    int create(OrderItem orderItem, Connection connection) throws SQLException;
}
