package com.webstore.repository;

import java.sql.Connection;
import java.sql.SQLException;

import com.webstore.entity.Order;
import com.webstore.entity.User;

public interface OrderRepository {
    int create(final int userId, final Connection connection) throws SQLException;

    Order find(final int id, final Connection connection) throws SQLException;
}
