package com.webstore.repository;

import java.sql.Connection;
import java.sql.SQLException;

import com.webstore.entity.User;

public interface CartRepository {
    boolean add(final User user, final Connection connection) throws SQLException;
}
