package com.webstore.repository.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import com.webstore.entity.OrderItem;
import com.webstore.entity.OrderState;
import com.webstore.repository.OrderItemRepository;
import com.webstore.repository.Transaction;

public class OrderItemRepositoryImpl implements OrderItemRepository {
    private static final String ID = "id";
    private static final String ORDER_ID = "order_id";
    private static final String PRODUCT_ID = "product_id";
    private static final String COUNT = "count";
    private static final String INSERT_ORDER_ITEM = "INSERT into order_item set " + ORDER_ID + "=?, "
            + PRODUCT_ID + "=?, " + COUNT + "=?";

    @Override
    public int create(OrderItem orderItem, Connection connection) throws SQLException {
        try {
            final PreparedStatement preparedStatement = connection.prepareStatement(INSERT_ORDER_ITEM, Statement.RETURN_GENERATED_KEYS);
            preparedStatement.setInt(1, orderItem.getOrderId());
            preparedStatement.setInt(2, orderItem.getProductId());
            preparedStatement.setInt(3, orderItem.getCount());
            preparedStatement.executeUpdate();
            ResultSet resultSet = preparedStatement.getGeneratedKeys();
            if (resultSet.first()) {
                return resultSet.getInt(1);
            }
        } catch (SQLException e) {
            Transaction.rollback(connection);
        } finally {
            Transaction.close(connection);
        }
        return -1;
    }
}
