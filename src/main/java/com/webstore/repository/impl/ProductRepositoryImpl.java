package com.webstore.repository.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.webstore.entity.Product;
import com.webstore.repository.ProductRepository;
import com.webstore.repository.Transaction;

public class ProductRepositoryImpl implements ProductRepository {
    private static final String ID = "id";
    private static final String NAME = "name";
    private static final String CATEGORY_ID = "category_id";
    private static final String PRICE = "price";
    private static final String DESCRIPTION = "description";
    private static final String PATH = "path";

    private static final String GET_ALL_PRODUCTS = "SELECT * FROM product";
    private static final String GET_PRODUCTS_IMAGES = "SELECT * FROM product_image WHERE product_id = ?";
    private static final String GET_PRODUCT = "SELECT * FROM product WHERE id = ?";
    private static final String GET_PRODUCTS_BY_CATEGORY = "SELECT * FROM product WHERE category_id = ?";
    private static final String IN_STOCK = "in_stock";

    @Override
    public Product getProduct(final int id, final Connection connection) throws SQLException {
        try {
            final PreparedStatement preparedStatement = connection.prepareStatement(GET_PRODUCT);
            preparedStatement.setInt(1, id);
            final ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                return extractProduct(resultSet);
            }
        } catch (SQLException e) {
            Transaction.rollback(connection);
        } finally {
            Transaction.close(connection);
        }
        return null;
    }

    @Override
    public List<Product> getProductsByCategoryId(int categoryId, Connection connection) throws SQLException {
        final List<Product> products = new ArrayList<>();
        try {
            final PreparedStatement preparedStatement = connection.prepareStatement(GET_PRODUCTS_BY_CATEGORY);
            preparedStatement.setInt(1, categoryId);
            final ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()){
                products.add(extractProduct(resultSet));
            }
        } catch (SQLException e) {
            Transaction.rollback(connection);
        } finally {
            Transaction.close(connection);
        }
        return products;
    }

    @Override
    public List<Product> getAll(final Connection connection) throws SQLException {
        final List<Product> products = new ArrayList<>();

        try {
            final Statement statement = connection.createStatement();
            final ResultSet resultSet = statement.executeQuery(GET_ALL_PRODUCTS);
            while (resultSet.next()){
                products.add(extractProduct(resultSet));
            }
        } catch (SQLException e) {
            Transaction.rollback(connection);
        } finally {
            Transaction.close(connection);
        }
        return products;
    }

    @Override
    public List<String> getProductImages(final int productId, final Connection connection) throws SQLException {
        final List<String> productImages = new ArrayList<>();

        try {
            final PreparedStatement preparedStatement = connection.prepareStatement(GET_PRODUCTS_IMAGES);
            preparedStatement.setInt(1, productId);
            final ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()){
                productImages.add(extractImage(resultSet));
            }
        } catch (SQLException e) {
            Transaction.rollback(connection);
        } finally {
            Transaction.close(connection);
        }
        return productImages;
    }

    private static Product extractProduct(ResultSet resultSet) throws SQLException {
        return new Product(
                resultSet.getInt(ID),
                resultSet.getString(NAME),
                resultSet.getInt(CATEGORY_ID),
                resultSet.getInt(PRICE),
                resultSet.getString(DESCRIPTION),
                resultSet.getBoolean(IN_STOCK));
    }

    private static String extractImage(ResultSet resultSet) throws SQLException {
        return resultSet.getString(PATH);
    }
}
