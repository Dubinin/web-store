package com.webstore.repository.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.webstore.entity.Category;
import com.webstore.repository.CategoryRepository;
import com.webstore.repository.Transaction;

public class CategoryRepositoryImpl implements CategoryRepository {
    private static final String ID = "id";
    private static final String NAME = "name";
    private static final String PARENT_CATEGORY_ID = "parent_category_id";
    private static final String IMAGE_PATH = "category_image_path";

    private static final String GET_ALL_MAIN_CATEGORIES = "SELECT * FROM category AS c WHERE c.parent_category_id is null";
    private static final String GET_CATEGORY = "SELECT * FROM category WHERE id = ?";
    private static final String GET_CHILDREN_CATEGORIES = "SELECT * FROM category WHERE parent_category_id = ?";

    @Override
    public Category getCategory(int id, Connection connection) throws SQLException {
        try {
            final PreparedStatement preparedStatement = connection.prepareStatement(GET_CATEGORY);
            preparedStatement.setInt(1, id);
            final ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next()){
                return extractCategory(resultSet);
            }
        } catch (SQLException e) {
            Transaction.rollback(connection);
        } finally {
            Transaction.close(connection);
        }
        return null;
    }

    @Override
    public List<Category> getChildrenCategories(int parentCategoryId, Connection connection) throws SQLException {
        final List<Category> categories = new ArrayList<>();
        try {
            final PreparedStatement preparedStatement = connection.prepareStatement(GET_CHILDREN_CATEGORIES);
            preparedStatement.setInt(1, parentCategoryId);
            final ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()){
                categories.add(extractCategory(resultSet));
            }
        } catch (SQLException e) {
            Transaction.rollback(connection);
        } finally {
            Transaction.close(connection);
        }
        return categories;
    }

    @Override
    public List<Category> getAllMainCategories(Connection connection) throws SQLException {
        final List<Category> categories = new ArrayList<>();

        try {
            final Statement statement = connection.createStatement();
            final ResultSet resultSet = statement.executeQuery(GET_ALL_MAIN_CATEGORIES);
            while (resultSet.next()) {
                categories.add(extractCategory(resultSet));
            }
        } catch (SQLException e) {
            Transaction.rollback(connection);
        } finally {
            Transaction.close(connection);
        }
        return categories;
    }

    private Category extractCategory(ResultSet resultSet) throws SQLException {
        return new Category(
                resultSet.getInt(ID),
                resultSet.getInt(PARENT_CATEGORY_ID),
                resultSet.getString(NAME),
                resultSet.getString(IMAGE_PATH));
    }
}
