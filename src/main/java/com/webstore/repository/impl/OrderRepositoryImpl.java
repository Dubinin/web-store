package com.webstore.repository.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import com.webstore.entity.Order;
import com.webstore.entity.OrderState;
import com.webstore.entity.User;
import com.webstore.repository.OrderRepository;
import com.webstore.repository.Transaction;

public class OrderRepositoryImpl implements OrderRepository {
    private static final String ID = "id";
    private static final String USER_ID = "user_id";
    private static final String STATE = "state";
    private static final String INSERT_ORDER = "INSERT into user_order set " + USER_ID + "=?," + STATE + "=?";
    private static final String GET_ORDER = "SELECT * FROM user_order WHERE " + ID + "=?";

    @Override
    public int create(int userId, Connection connection) throws SQLException {
        try {
            final PreparedStatement preparedStatement = connection.prepareStatement(INSERT_ORDER, Statement.RETURN_GENERATED_KEYS);
            preparedStatement.setInt(1, userId);
            preparedStatement.setString(2, OrderState.NEW.name());
            preparedStatement.executeUpdate();
            ResultSet resultSet = preparedStatement.getGeneratedKeys();
            if (resultSet.first()) {
                return resultSet.getInt(1);
            }
        } catch (SQLException e) {
            Transaction.rollback(connection);
        } finally {
            Transaction.close(connection);
        }
        return -1;
    }

    @Override
    public Order find(int id, Connection connection) throws SQLException {
        try {
            final PreparedStatement preparedStatement = connection.prepareStatement(GET_ORDER);
            preparedStatement.setInt(1, id);
            final ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                return extractOrder(resultSet);
            }
        } catch (SQLException e) {
            Transaction.rollback(connection);
        } finally {
            Transaction.close(connection);
        }
        return null;
    }

    private static Order extractOrder(final ResultSet resultSet) throws SQLException {
        return new Order(
                resultSet.getInt(ID),
                resultSet.getInt(USER_ID),
                OrderState.valueOf(resultSet.getString(STATE)));
    }
}
