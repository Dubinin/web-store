package com.webstore.repository.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.webstore.entity.Role;
import com.webstore.entity.User;
import com.webstore.repository.Transaction;
import com.webstore.repository.UserRepository;

public class UserRepositoryImpl implements UserRepository {
    private static final String ID = "id";
    private static final String FIRST_NAME = "first_name";
    private static final String LAST_NAME = "last_name";
    private static final String EMAIL = "email";
    private static final String ADDRESS = "address";
    private static final String PHONE_NUMBER = "phone_number";
    private static final String PASSWORD = "password";
    private static final String ROLE = "role";
    private static final String GET_USER = "SELECT * FROM user WHERE " + EMAIL + " = ? AND " + PASSWORD + " = ?";
    private static final String INSERT_USER = "INSERT into user set " + FIRST_NAME + "=?," + LAST_NAME + "=?,"
            + EMAIL + "=?," + ADDRESS + "=?," + PHONE_NUMBER + "=?," + PASSWORD + "=?," + ROLE + "=?";

    @Override
    public User findUser(final String email, final String password, final Connection connection) throws SQLException {
        try {
            final PreparedStatement preparedStatement = connection.prepareStatement(GET_USER);
            preparedStatement.setString(1, email);
            preparedStatement.setString(2, password);
            final ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                return extractUser(resultSet);
            }
        } catch (SQLException e) {
            Transaction.rollback(connection);
        } finally {
            Transaction.close(connection);
        }
        return null;
    }

    @Override
    public boolean create(final User user, final Connection connection) throws SQLException {
        try {
            final PreparedStatement preparedStatement = connection.prepareStatement(INSERT_USER);
            preparedStatement.setString(1, user.getFirstName());
            preparedStatement.setString(2, user.getLastName());
            preparedStatement.setString(3, user.getEmail());
            preparedStatement.setString(4, user.getAddress());
            preparedStatement.setString(5, user.getPhoneNumber());
            preparedStatement.setString(6, user.getPassword());
            preparedStatement.setString(7, user.getRole().name());
            return preparedStatement.executeUpdate() > 0;
        } catch (SQLException e) {
            Transaction.rollback(connection);
        } finally {
            Transaction.close(connection);
        }
        return false;
    }

    private static User extractUser(final ResultSet resultSet) throws SQLException {
        return new User(
                resultSet.getInt(ID),
                resultSet.getString(FIRST_NAME),
                resultSet.getString(LAST_NAME),
                resultSet.getString(EMAIL),
                resultSet.getString(ADDRESS),
                resultSet.getString(PHONE_NUMBER),
                resultSet.getString(PASSWORD),
                Role.valueOf(resultSet.getString(ROLE)));
    }

}
