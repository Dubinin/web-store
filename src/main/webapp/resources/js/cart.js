(function () {
    var math_it_up = {
        '+': function (x, y) { return x + y },
        '-': function (x, y) { return x - y }
    };

    $(document).ready(function() {
        var products = CartUtil.getProducts();
        var cartItemContainer = $("#cart-item-container");
        var cartItemTemplate = cartItemContainer.find("#html-template").parent().html();
        var totalPrice = 0;
        for (var i = 0; i < products.length; i++) {
            $.get( "/product", {'id' : products[i]}, function( data ) {
                cartItemContainer.append(createCartItem(data, cartItemTemplate));
            });
        }

        addEventHandlers();
        // это уже совсем капец
        setTimeout(onCartCountUpdate, 1000);
    });

    function addEventHandlers() {
        $("#cart-item-container").on("click", ".qty-minus, .qty-plus", onCartCounterBtn);
        $(document).on("card-counter-update", onCartCountUpdate);
        $("#make-order").on("click", onMakeOrder);
    }

    function onMakeOrder() {
        var data = [];
        var products = $("[data-id]:not(#html-template)");
        for (var i = 0; i < products.length; i++) {
            var $product = $(products[i]);
            data.push({id: $product.data("id"), count: $product.data("count")});
        }

        $.post( "/cart", {"cart" : JSON.stringify(data)}, function() {
            CartUtil.clean();
            window.location.replace("/");
        });
    }

    function onCartCountUpdate() {
        var totalPriceField = $("#total-price");
        var deliveryPriceField = $("#delivery-price");
        var subtotalPrice = $("#subtotal-price");
        var subtotal = 0;
        var delivery = 0;

        var items = $("[data-price]:not(#html-template)");

        for (var i = 0; i < items.length; i++) {
            var $item = $(items[i]);
            var price = Number($item.data("price"));
            var count = Number($item.find("input").val());
            subtotal += (price * count);
        }

        delivery = subtotal > 500 ? 0 :  parseInt(subtotal * 0.05);

        subtotalPrice.text("$" + subtotal);
        deliveryPriceField.text("$" + delivery);
        totalPriceField.text("$" + (subtotal + delivery));
    }

    function onCartCounterBtn(e) {
        var item = $(e.target);
        var id = item.parents("tr").data('id');
        var operation = item.data("operation");
        operation = operation ? operation : $(e.target).parent().data("operation");
        var input = item.parents(".quantity").find("input");
        var count = Number(input.val());

        var result = math_it_up[operation](count, 1);
        if (result < 1) {
            $(e.target).parents("tr").remove();
        } else {
            input.val(result);
            $(e.target).parents("tr").data("count", result);
        }

        if (operation == "-") {
            CartUtil.remove(id);
        } else {
            CartUtil.add(id);
        }

        $(document).trigger("card-counter-update");
    }

    function createCartItem(item, template) {
        var config = [
            {"name" : "image", "value" : item.images[0]},
            {"name" : "count", "value" : CartUtil.getCountById(item.id)},
            {"name" : "price", "value" : item.price},
            {"name" : "name", "value" : item.name},
            {"name" : "id", "value" : item.id}];
        return TemplateUtil.create(template, config);
    }
}());