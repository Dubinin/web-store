(function () {
    $(document).ready(function() {
        var btn = $("#checkin");

        if (getCookie("shop_user")) {
            btn.text("Log-out");
            btn.attr("href", "#");
            btn.on("click", function() {
                $.post("/log-out", function() {
                    deleteCookie("shop_user");
                    window.location.replace("/checkin.html");
                });
            });
        }
    });

    var deleteCookie = function(name) {
        document.cookie = name + '=;expires=Thu, 01 Jan 1970 00:00:01 GMT;';
    };

    function getCookie(name) {
      var value = "; " + document.cookie;
      var parts = value.split("; " + name + "=");
      if (parts.length == 2) return parts.pop().split(";").shift();
    }
}());