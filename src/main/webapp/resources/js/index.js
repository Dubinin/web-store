(function () {
    $(document).ready(function() {
        $.get( "/category", function( data ) {
            for (var i = 0; i < data.length; i++) {
                var categoryItem = createCategoryItem(data[i]);
                $(".amado-pro-catagory").append(categoryItem);
            }
            $(document).trigger('activate');
        });
    });

    function createCategoryItem(category) {
        return '<div class="single-products-catagory clearfix">' +
               '<a href="shop.html?category_id=' + category.id + '"><img src="' + category.imagePath + '" alt="">' +
               '<div class="hover-content"><div class="line"></div>' +
               '<h4>' + category.name + '</h4></div></a></div>';
    }
}());