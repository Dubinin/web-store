(function () {
    $(document).ready(function() {
        updateCardCounter();
    });

    $(document).on("card-counter-update", function() {
        updateCardCounter();
    });

    function updateCardCounter() {
        var count = CartUtil.getCount();
        $(".cart-counter").text("(" + count + ")");
    }
}());