(function () {
    $(document).ready(function() {
        var url = new URL(window.location.href);
        initCategoryBlock(url);
        initProductsBlock(url);
        initEventHandlers();
    });

    function initEventHandlers() {
        $(document).on("click", ".add-to-card-btn", onAddToCard);
    }

    function onAddToCard(e) {
        var productId = $(e.target).parent().data("id");
        CartUtil.add(productId);
        $(document).trigger("card-counter-update");
    }

    function initCategoryBlock(url) {
        var params = [
            {name: "id", value : url.searchParams.get("category_id")}
        ];

        var requestUrl = createUrl("/category", params);
        var menu = $("#menu-items");
        $.get( requestUrl, function( data ) {
            for (var i = 0; i < data.length; i++) {
                var item = createMenuItemByCategory(data[i]);
                menu.append(item);
            }
        });
    }

    function initProductsBlock(url) {
        var params = [
            {name: "category_id", value : url.searchParams.get("category_id")}
        ];

        var requestUrl = createUrl("/product", params);
        var productTemplate = $("#html-template").parent().html();

        $.get( requestUrl, function( data ) {
            for (var i = 0; i < data.length; i++) {
                var productItem = createProductItem(data[i], productTemplate);
                $(".product-items").append(productItem);
            }
            $(document).trigger('activate');
        });
    }

    function createUrl(endpoint,  params) {
        var requestUrl = endpoint + (params.length > 0 ? "?" : "");
        for (var i = 0; i < params.length; i++) {
            if (params[i].value) {
                requestUrl += params[i].name + "=" + params[i].value;
            }
            if (i < params.length -1) {
                requestUrl += "&";
            }
        }
        return requestUrl;
    }

    function createProductItem(product, template) {
        var config = [
            {"name" : "image1", "value" : product.images[0]},
            {"name" : "price", "value" : product.price},
            {"name" : "name", "value" : product.name},
            {"name" : "hidden", "value" : ""},
            {"name" : "id", "value" : product.id}];

        return TemplateUtil.create(template, config);
    }

    function createMenuItemByCategory(category) {
        return createMenuItem("/shop.html?category_id=" + category.id, category.name);
    }

    function createMenuItem(path, name) {
        return "<li><a href='" + path + "'>" + name + "</a></li>";
    }
}());