(function () {
    $(document).ready(function() {
        var url = new URL(window.location.href);
        var id = url.searchParams.get("id");
        $.get( "/product", {'id' : id}, function( data ) {
            putProductName(data);
            putProductPrice(data);
            putProductDescription(data);
            putInStock(data);
            putImages(data);

            activateAddToCartBtn(data);
            initListenerAddToCartBtn();

            $(document).trigger('activate');
        });
    });

    function initListenerAddToCartBtn() {
        $('[data-product="add-to-cart"]:not(.amado-btn-disabled)').on('click', function(e) {
            var url = new URL(window.location.href);
            CartUtil.add(url.searchParams.get("id"));
            $(document).trigger("card-counter-update");
        });
    }

    function activateAddToCartBtn(data) {
        $('[data-product="add-to-cart"]').addClass(data.inStock ? 'amado-btn' : 'amado-btn-disabled');
    }

    function putInStock(data) {
        $('[data-product="in-stock"]').addClass(data.inStock ? 'avaibility' : 'unavailable');
    }

    function putProductName(data) {
        $('[data-product="name"]').text(data.name);
    }

    function putProductPrice(data) {
        $('[data-product="price"]').text('$' + data.price);
    }

    function putProductDescription(data) {
        $('[data-product="description"]').text(data.description);
    }

    function putImages(data) {
        var imageCarousel = $(".carousel-inner");
        var indicatorContainer = $('.carousel-indicators');
        var images = data.images;
        for (var i = 0; i < images.length; i++) {
            var carouselItem = '<div class="carousel-item' + (i == 0 ? ' active' : '') + '">' +
            '<a class="gallery_img" href="' + images[i] + '">' +
            '<img class="d-block w-100" src="' + images[i] + '" alt="slide">' +
            '</a></div>';
            imageCarousel.append(carouselItem);

            var indicator = '<li class="' + (i == 0 ? ' active' : '') + '" data-target="#product_details_slider" data-slide-to="' + i + '" style="background-image: url(' + images[i] + ');"></li>';
            indicatorContainer.append(indicator);
        }
    }
}());