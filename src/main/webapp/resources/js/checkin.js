(function () {
    $(document).ready(function() {
        $(document).trigger('activate');
        initFormsAndBtns();
        initEventListeners();
    });

    function initFormsAndBtns() {
        hideAllForms();
        var activeForm = $(".active[data-form]").data("form");
        $("." + activeForm).show();
    }

    function initEventListeners() {
        $(document).on("submit", ".checkin-form", onFormSubmit);
        $(".form-control-btn").on("click", onFormShowHide);
    }

    function onFormShowHide(e) {
        hideAllForms();
        deactivateAllFormControlBtns();
        var btn = $(e.target);
        btn.addClass("active");
        $("." + btn.data("form")).show();
    }

    function hideAllForms() {
        $(".log-in, .sign-in").hide();
    }

    function deactivateAllFormControlBtns() {
        $(".form-control-btn").removeClass("active");
    }

    function onFormSubmit(e) {
        e.preventDefault();
        var $form = $(e.target);
        var url = $form.attr("action");
        var params = {};
        var inputs = $form.find("input");
        for (var i = 0; i < inputs.length; i++) {
            params[$(inputs[i]).data("field")] = $(inputs[i]).val();
        }

        $.post(url, params, function() {
            window.location.replace(document.referrer);
        });
    }
}());