function CartUtil() {};

CartUtil.add = function(id) {
    var cartJson = window.localStorage.getItem("cart");
    if (cartJson) {
        var cart = JSON.parse(cartJson);
        var item = CartUtil.getItemCardById(id, cart);
        if (item != null) {
            item.count = item.count + 1;
        } else {
            cart.push({"id" : id, "count" : 1});
        }
        window.localStorage.setItem("cart", JSON.stringify(cart));
    } else {
        window.localStorage.setItem("cart", JSON.stringify([{"id" : id, "count" : 1}]));
    }
}

CartUtil.remove = function(id) {
    var cartJson = window.localStorage.getItem("cart");
    if (cartJson) {
        var cart = JSON.parse(cartJson);
        var item = CartUtil.getItemCardById(id, cart);
        if (item != null) {
            if (item.count > 1) {
                item.count = item.count - 1;
            } else {
                cart.splice(cart.indexOf(item), 1);
            }
        }
        window.localStorage.setItem("cart", JSON.stringify(cart));
    }
}

CartUtil.clean = function() {
    window.localStorage.setItem("cart", []);
}

CartUtil.getCount = function() {
    var cartJson = window.localStorage.cart;
    var count = 0;
    if (cartJson) {
        var cart = JSON.parse(cartJson);
        if (cart) {
            for (var i = 0; i < cart.length; i++) {
                var item = cart[i];
                count += item.count;
            }
        }
    }
    return count;
}

CartUtil.getCountById = function(id) {
    var cartJson = window.localStorage.cart;
    var count = 0;
    if (cartJson) {
        var cart = JSON.parse(cartJson);
        if (cart) {
            for (var i = 0; i < cart.length; i++) {
                var item = cart[i];
                if (item.id == id) {
                    return item.count;
                }
            }
        }
    }
    return 0;
}

CartUtil.getItemCardById = function (id, cart) {
    for (var i = 0; i < cart.length; i++) {
        var cartItem = cart[i];
        if (cartItem.id == id) {
            return cartItem;
        }
    }
    return null;
}

CartUtil.getProducts = function () {
    var productsId = [];
    var cartJson = window.localStorage.cart;
    if (cartJson) {
        var cart = JSON.parse(cartJson);
        if (cart) {
            for (var i = 0; i < cart.length; i++) {
                var item = cart[i];
                    productsId.push(item.id);
                }
            }
    }
    return productsId;
}