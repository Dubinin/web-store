function TemplateUtil() {};

TemplateUtil.create = function(template, data) {
    var result = template;
    for (var i = 0; i < data.length; i++) {
        var itemConfig = data[i];
        result = TemplateUtil.replaceAll(result, "${" + itemConfig.name + "}", itemConfig.value);
    }
    return TemplateUtil.replaceAll(result, 'id="html-template', "");
}

TemplateUtil.replaceAll = function (template, expression, value) {
    return template.split(expression).join(value);
}